<?php
namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Models\ProductCategoryModel;
use App\Modules\Admin\Models\ProductModel;
use App\Modules\Admin\Models\ProductStockLogModel;
use App\Modules\Libraries\Alert;
use App\Modules\Libraries\Breadcrumb;
use Illuminate\Support\Facades\Auth;

class Product extends GenesisController {
    public function __construct()
    {
        $this->middleware('admin_role:product');
        parent::__construct();

        $this->model    = new ProductModel;
    }

    public function index() {
        $this->set_page_title('Product Management');
        $admin    = Auth::guard('admin')->user();
        if($this->_act == 'add') {
            $this->after_save  = 'createInitLog';
        } else if($this->_act == 'edit') {
            $this->before_save  = 'checkStock';
        }

        $category   = $this->_get_filter_select(new ProductCategoryModel, 'category_name');
        $this->data['category'] = $category;

        return $this->init('product.product_grid');
    }

    protected function checkStock($record) {
        $oldRecord  = ProductModel::find($record->id);

        if($oldRecord->stock == $record->stock) {
            return true;
        } else {
            $log    = new ProductStockLogModel;
            $log->product_id    = $record->id;
            $log->stock         = $record->stock;
            $log->admin_id      = Auth::guard('admin')->user()->id;
            $log->description   = 'Update';
            $log->save();
        }
        return true;
    }


    protected function createInitLog($record) {
        $log    = new ProductStockLogModel;
        $log->product_id    = $record->id;
        $log->stock         = $record->stock;
        $log->admin_id      = Auth::guard('admin')->user()->id;
        $log->description   = 'Init';
        $log->save();

        return true;
    }

    public function gapakeBuilder() {
        return $this->render_view('product.product_tanpa_builder');
    }

    public function detail($id) {
        $product    = ProductModel::find($id);
        if(!$product) {
            Alert::add('GA KETEMU WOI!');
            return redirect()->back();
        }
        $this->model    = new ProductStockLogModel;
        $this->model    = $this->model->where('product_id', $product->id);
        $this->removeDefaultButton('add');
        $this->addDefaultButton('Coba', [
            'type'=>'link',
            'route'=>'admin_dashboard',
            'image'=>'fa-arrow-alt-circle-right'
        ]);
        $this->addBackButton('admin_product');
        $this->set_page_title($product->product_name.' Stock Log');
        Breadcrumb::add('Stock Log');
        $this->removeActionButtonAndSendToData('edit');
        $this->removeActionButtonAndSendToData('delete');

        return $this->init('product.stock_log_grid');
    }
}
