<?php
namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Models\ProductCategoryModel;

class Category extends GenesisController {
    public function __construct()
    {
        $this->middleware('admin_role:product_category');
        parent::__construct();
        $this->set_page_title('Product Category Management');
        $this->model    = new ProductCategoryModel;
    }

    public function index() {
        return $this->init('product.category_grid');
    }
}
