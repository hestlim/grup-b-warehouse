<?php

namespace App\Modules\Admin\Models;

class ProductStockLogModel extends CoreGenesisModel {
    protected $table    = 'product_stock_log';
    protected $with     = ['admin'];

    public function admin() {
        return $this->hasOne(AdministratorModel::class, 'id', 'admin_id');
    }
}
