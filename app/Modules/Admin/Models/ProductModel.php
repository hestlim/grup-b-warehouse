<?php
namespace App\Modules\Admin\Models;

class ProductModel extends CoreGenesisModel {
    protected $table    = 'product';
    protected $with     = ['category'];

    public function category() {
        return $this->hasOne(ProductCategoryModel::class, 'id', 'category_id');
    }
}
