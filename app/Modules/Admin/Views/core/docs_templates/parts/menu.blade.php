<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 23/02/2018
 * Time: 11:18
 */
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="bg-warning">
                <a href="{{ route('admin_dashboard') }}"><i class="far fa-arrow-left"></i>&nbsp;Back to Dashboard</a>
            </li>
            @foreach($docs as $nav)
{{--                <li class="{{ ($nav->permalink === $detail->permalink || $nav->findChildren($detail->permalink)) ? 'active' : '' }}{{ $nav->children(1)->count() ? " treeview nav-item dropdown" : "" }}">--}}
{{--                    <a href=" {{ $nav->children(1)->count() ? "#" : route('admin_system_docs_detail', $nav->permalink) }}">--}}
{{--                        <span>{{ $nav->menu }}</span>--}}
{{--                        @if($nav->children(1)->count())<i class="fa fa-angle-left pull-right"></i>@endif--}}
{{--                    </a>--}}
{{--                    @if($nav->children(1)->count())--}}
{{--                        <ul class="treeview-menu">--}}
{{--                            @foreach($nav->children(1)->get() as $subNav)--}}
{{--                                <li class="{{ $subNav->permalink === $detail->permalink ? 'active ' : '' }}">--}}
{{--                                    <a class="nav-link" href="{{ route('admin_system_docs_detail', $subNav->permalink) }}">--}}
{{--                                        <span>{{ $subNav->menu }}</span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            @endforeach--}}
{{--                        </ul>--}}
{{--                    @endif--}}
{{--                </li>--}}



                <?php
                $__active   = ($nav->permalink === $detail->permalink || $nav->findChildren($detail->permalink));
                ?>
                @if(!$nav->children(1)->count())

                    <li>
                        <a class="nav-link {{ $__active ? 'active' : '' }}" href="{{ $nav->children(1)->count() ? "#" : route('admin_system_docs_detail', $nav->permalink) }}">
                            <span style="font-size:12px;">&nbsp;{{ $nav->menu }}</span>
                        </a>
                    </li>
                @else
                    <li class="nav-item dropdown {{ $__active ? 'active' : '' }}">
                        <a href="#" class="nav-link has-dropdown">
                            <span style="font-size:12px;">&nbsp;{{ $nav->menu }}</span>
                        </a>
                        <ul class="dropdown-menu">

                            @foreach($nav->children(1)->get() as $subNav)
                                <li>
                                    <a class="nav-link {{ $subNav->permalink === $detail->permalink ? 'active ' : '' }}" href="{{ route('admin_system_docs_detail', $subNav->permalink) }}">
                                        <span style="font-size:12px;">&nbsp;{{ $subNav->menu }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endif





            @endforeach
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
