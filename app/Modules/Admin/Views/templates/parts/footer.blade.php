<footer class="main-footer">
    <div class="footer-left">
        <small>Copyright &copy; {{ date('Y') }} Kim Punya!.</small>
    </div>
    <div class="footer-right">
        <small>v{{ $backend_version }}</small>
    </div>
</footer>