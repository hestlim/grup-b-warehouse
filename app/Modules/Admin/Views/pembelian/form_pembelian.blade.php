@extends("admin::templates.master")

@section('scripts')
    {!! \App\Modules\Libraries\Plugin::get('datepicker') !!}
    <script type="text/javascript">
        $("#order_date").datepicker();

        var $modalHolder        = $("#modal-product");//selector uuntuk modal
        var $tableCartHolder    = $("#table-cart");//selector untuuk table keranjang
        var $btnSelectProduct   = $("#btn-select-product");//tombol add product

        var cart    = [];//id, stock, data nama, attribute lainnya// ini keranjang
        var rawProduct  = [];//ini tujuannya buat simpen raw data dari produk
        //notes: kalau productnya ada banyak (ribuan keatas) hati2 komputer client meledak nanti

        //ini trigger ketika tombol add product di click akan ajax ke ke admin service get product
        $btnSelectProduct.on('click', function () {
            //ajax
            if(rawProduct.length <= 0) {
                $.get("{{ route('admin_service_get_product') }}", function (result) {//disini ajaxnya pake method get
                    if(result.status) {//disini liat rresult nyaa
                        rawProduct  = result.others.product;//tampung resultnya ke temporary variable
                        renderTable();//render table untuk nampilin list product buat bsa ddi select oleh user
                    } else {
                        bootbox.alert('ga bisa');//ini kalau gagal tampillin alert
                    }
                });
            } else {
                renderTable();//kalau udah pernah fetch data product, tinggal render aja
            }

            $modalHolder.modal('show');
        });

        function renderTable() {
            var $temp='<table class="table">';
            $temp+='<tbody>';
            $.each(rawProduct, function (index, el) {
                $temp+='<tr>';
                $temp+='<td>'+el.product_code+'</td>';
                $temp+='<td>'+el.product_name+'</td>';
                $temp+='<td>'+el.category.category_name+'</td>';
                $temp+='<td><a href="#" class="select-product" data-id="'+el.id+'"><i class="far fa-hand-point-up"></i></a></td>';
                $temp+='</tr>';
            });
            $temp+='</tbody></table>';

            $modalHolder.find('.modal-body').html($temp);//ini modalnya dicari modalbody kemudian diisi
        }


        $('body').on('click', '.select-product', function () {//ini triger buat cclick productnya yang ada di dalam list product ketika tambah prooduct
            var el  = $(this);
            var id  = $(this).data('id');

            //dicheck di cart itu dah ada id tssb atau belum?

            //id, qty, modal
            var detailKeranjang = [id, 0, 0];//format kernajang belanja, hanya butuh id qty modal.
            cart.push(detailKeranjang);//simpan ke cart
            $modalHolder.modal('hide');//tutup modal
            renderCart();//tampilin di keranjang belanja
        });

        function renderCart() {
            var temp    = '';
            $.each(cart, function (index, el) {
                var data = $.grep(rawProduct, function(v) {//ini prroses ngambil raw data product
                    return v.id === el[0];
                });

                if(data.length <= 0) return;
                data    = data[0];

                temp    += '<tr>';
                temp    += '<td>'+(parseInt(index)+1)+'</td>';
                temp    += '<td>'+data.product_code+' - '+data.product_name+'</td>';
                temp    += '<td>'+data.category.category_name+'</td>';
                temp    += '<td width="50"><input type="number" data-id="'+data.id+'" class="input-qty form-control" value="'+el[1]+'" /></td>';
                temp    += '<td><input type="number" data-id="'+data.id+'" class="input-modal form-control" value="'+el[2]+'" /></td>';
                temp    += '<td><span class="input-total" data-id="'+data.id+'"></span></td>';
                temp    += '</tr>';
            });

            $tableCartHolder.find('tbody').html(temp);//
        }


        $("body").on('change', '.input-modal', function () {//proses kketika input modal berubah
            var selector    = $(this);
            var id          = selector.data('id');
            var modal       = selector.val();
            var qty         = $(".input-qty[data-id='"+id+"']").val();

            var selectedCart = $.grep(cart, function(v) {//disini ambil dan sessuaikan ddengan ID yg teerpilih dan update ccartnya
                if(v[0] === id) {
                    v[1]        = qty;
                    v[2]        = modal;
                }
            });

            var total   = modal*qty;
            $(".input-total[data-id='"+id+"']").html(numberWithCommas(total));
        });

        $("body").on('change', '.input-qty', function () {//ini sama seperti atas, tapi bagian qty.
            var selector    = $(this);
            var id          = selector.data('id');
            var qty         = selector.val();
            var modal       = $(".input-modal[data-id='"+id+"']").val();

            var selectedCart = $.grep(cart, function(v) {
                if(v[0] === id) {
                    v[1]        = qty;
                    v[2]        = modal;
                }
            });

            var total   = modal*qty;
            $(".input-total[data-id='"+id+"']").html(numberWithCommas(total));
        });



        $("#btn-submit").on('click', function () {//tombol submit
            var data    = {//simpan datanya ke variable semua
                _token:'{{ csrf_token() }}',
                cart:cart,
                order_code:$(".text_order_code").val(),
                order_date:$(".text_order_date").val(),
            };
//lalu kirim melalui method post
            $.post('{{ route('admin_simpan_form_pembelian') }}', data, function (respond) {
                console.log(respond);//disini perlu di disabled tombol; submit, biar user ga double click,
                //kalau perlu tmapilin loading.
                //kalau berhasil. redirect pagenya. atau reffresh.
            });
        });
    </script>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="row pb-5 p-5">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="order_code">Order Code</label>
                                <input name="order_code" type="text" class="text_order_code form-control" id="order_code" aria-describedby="order_code" placeholder="Enter order code">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="order_date">Order Date</label>
                                <input type="text" name="order_date" class="text_order_date form-control" id="order_date" aria-describedby="order_date" placeholder="Enter Date">
                            </div>
                        </div>
                    </div>

                    <div class="row p-5">
                        <div class="col-md-12">
                            <button class="btn btn-primary mb-5" id="btn-select-product"><i class="far fa-plus"></i>&nbsp;Tambah Product</button>

                            <table class="table" id="table-cart">
                                <thead>
                                <tr>
                                    <th class="border-0 text-uppercase small font-weight-bold">No.</th>
                                    <th class="border-0 text-uppercase small font-weight-bold">Item</th>
                                    <th class="border-0 text-uppercase small font-weight-bold">Category</th>
                                    <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
                                    <th class="border-0 text-uppercase small font-weight-bold">Unit Cost</th>
                                    <th class="border-0 text-uppercase small font-weight-bold">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <button type="button" id="btn-submit" class="btn btn-success text-center">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('modal_holder')
    <div class="modal fade bd-example-modal-lg" id="modal-product" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Select Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </div>
    </div>

@stop
