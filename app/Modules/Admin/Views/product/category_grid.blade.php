@extends('admin::templates.master')

@section('scripts')
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Category Name',width:11,data:'category_name', align:'left'},
                {head:'Publish',width:"1",data:'publish',align:'center',sort:true,type:'check'}
            ];

            var filter  = [];

            var button  = {
                add:[{
                    name:'category_name',
                    label:'Category Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Category Name'
                }],
                edit:[{
                    name:'category_name',
                    label:'Category Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Category Name'
                }]
            };


            i_form.initGrid({
                header: header,
                filter: [],
                button: button,
                sort: {{ $menu_default_sort }},
                data: {!! json_encode($records->toArray()) !!},
                pagination: '{!! $pagination !!}',
                menu_action: {!! json_encode($menu_action) !!}
            },$("#grid"));
        });
    </script>
@stop
