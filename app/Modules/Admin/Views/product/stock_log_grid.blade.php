@extends('admin::templates.master')

@section('scripts')
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Stock',width:5,data:'stock', align:'left'},
                {head:'Desc',width:5,data:'description', align:'left'},
                {head:'By',width:5,data:'admin_id', align:'left', type:'relation',belongsTo:['admin','username']},
                {head:'Date',width:2,data:'created_at', align:'left'},
            ];


            i_form.initGrid({
                header: header,
                filter: [],
                button: [],
                sort: {{ $menu_default_sort }},
                data: {!! json_encode($records->toArray()) !!},
                pagination: '{!! $pagination !!}',
                menu_action: {!! json_encode($menu_action) !!}
            },$("#grid"));
        });
    </script>
@stop
