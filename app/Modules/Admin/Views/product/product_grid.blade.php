@extends('admin::templates.master')

@section('scripts')
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Category Name',width:2,data:'category_id',sort:true, align:'left',type:'relation',belongsTo:['category','category_name']},
                {head:'Code',width:2,data:'product_code',align:'center',sort:true},
                {head:'Product Name',width:2,data:'product_name',align:'center',sort:true},
                {head:'Stock',width:2,data:'stock',align:'center',sort:true},
                {head:'Publish',width:2,data:'publish',align:'center',sort:true,type:'check'},
                {head:'',width:"1",type:'custom',align:'center',render:function (records, value) {
                        return '<a href="{{ url($__admin_path.'/product') }}/'+records.id+'">detail</a>';
                    }}
            ];

            var filter  = [
                {data:'category_id',type:'select', options: $.parseJSON('{!! json_encode($category) !!}')},
                {data:'product_code',type:'text'}
            ];

            var button  = {
                add:[{
                    name:'category_id',
                    label:'Category',
                    type:'select',
                    data:{!! json_encode($category) !!},
                    required:true
                },{
                    name:'product_code',
                    label:'Product Code',
                    type:'text',
                    required:true,
                    placeholder:'Input Product Code'
                },{
                    name:'product_name',
                    label:'Product Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Product Name'
                }, {
                    name:'stock',
                    label:'Stock',
                    type:'number',
                    required: true,
                    placeholder: 'Input Stock'
                },{
                    name:'gender',
                    label:'Gender',
                    type:'radio',
                    toggle:true,
                    required:true,
                    option:[
                        {value:"male",display:"Male",toggle:'test'},
                        {value:"female",display:"Female"}
                    ]
                }, {
                    name:'test',
                    type:'panel',
                    id:'test',
                    field: [{
                        name:'stock',
                        label:'Stock',
                        type:'number',
                        required: true,
                        placeholder: 'Input Stock'
                    }]
                }],
                edit:[{
                    name:'category_id',
                    label:'Category',
                    type:'select',
                    data:{!! json_encode($category) !!},
                    required:true
                },{
                    name:'product_code',
                    label:'Product Code',
                    type:'text',
                    required:true,
                    placeholder:'Input Product Code'
                },{
                    name:'product_name',
                    label:'Product Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Product Name'
                }, {
                    name:'stock',
                    label:'Stock',
                    type:'number',
                    required: true,
                    placeholder: 'Input Stock'
                }]
            };


            i_form.initGrid({
                number: true,
                header: header,
                filter: filter,
                button: button,
                sort: {{ $menu_default_sort }},
                data: {!! json_encode($records->toArray()) !!},
                pagination: '{!! $pagination !!}',
                menu_action: {!! json_encode($menu_action) !!}
            },$("#grid"));
        });
    </script>
@stop
